//Based on this:  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes#Class_declarations
window.alert("Hello World!");
class Rectangle {
    constructor(height, width) {
        this.height = height;
        this.width = width;
        this.amethod();
    }
    amethod() { 
        console.log("I am the amethod."); 
        console.log(this.height);
    }
}
var r = new Rectangle(2,2);
console.log(r);
r.amethod();
window.alert("Goodbye World!");
